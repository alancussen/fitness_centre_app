Rails.application.routes.draw do
  get "/static_pages/:page" => "static_pages#show"
  
  #get 'static_pages/home'

  #get 'static_pages/membership'

  #get 'static_pages/classes'

  #get 'static_pages/contactus'
  
  root 'static_pages#home'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
