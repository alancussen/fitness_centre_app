require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  test "should get home" do
    get static_pages_home_url
    assert_response :success
  end

  test "should get membership" do
    get static_pages_membership_url
    assert_response :success
  end

  test "should get classes" do
    get static_pages_classes_url
    assert_response :success
  end

  test "should get contactus" do
    get static_pages_contactus_url
    assert_response :success
  end

end
